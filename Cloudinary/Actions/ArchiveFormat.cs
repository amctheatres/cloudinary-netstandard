﻿using System.ComponentModel.DataAnnotations;

namespace CloudinaryDotNet.Actions
{
    /// <summary>
    /// The format for the generated archive
    /// </summary>
    public enum ArchiveFormat
    {
        /// <summary>
        /// Specifies ZIP format for an archive
        /// </summary>
        [Display(Description = "zip")]
        Zip
    }
}
